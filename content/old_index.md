---
title: "Welcome!"
description: "nbuton website homepage"
showToc: true
layout: homepage
---

***~ Welcome to my scientific website!***

<!-- <img src="/img/portrait.jpg" alt="Avatar" height="200"> -->

<!-- ## ✨ Highlighted pages -->

## 🗺️ Website map

> 🌐 The language of the contents is detailed by EN and FR for respectively the English and the French languages

🏡 [**Home**](/) This homepage

<!-- 🖋️ [**Posts**](/posts) Some posts (EN/FR) -->

<!-- 💻 [**Software**](/software) List of developed software (EN) -->

👤 [**About**](/about) Who am I? (EN)
