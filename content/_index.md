---
title: "Who am I?"
description: "Young researcher"
date: 2023-11-23
layout: homepage
tags:
- "research"
---

I am a postdoctoral researcher at [IBISC](https://www.ibisc.univ-evry.fr/) (FR), in the [AROBAS team](https://www.ibisc.univ-evry.fr/equipe/arobas/).
I am working on a Graph Neural Network (GNN) to integrate multi-omics data to predict the stages of Parkinson's disease.

## 🔗 Some links

<!-- 📜 **CV** [Nicolas Buton](assets/cv_nicolas_buton.pdf) -->

🆔 **ORCID** [0000-0002-4079-5501](https://orcid.org/0000-0002-4079-5501)

💼 **LinkedIn** [Nicolas Buton](https://fr.linkedin.com/in/nicolasbuton)

<!-- 🦣 **Mastodon** <a rel="me" href="https://social.sciences.re/@nbuton" target="_blank">@nbuton@sciences.re</a> -->

𝕏 **Twitter** [@NicolasButon1](https://twitter.com/NicolasButon1)

🦊 **GitLab** [nbuton](https://gitlab.com/nbuton/)

🐱 **GitHub** [nbuton](https://github.com/nbuton)


## 📫 Contact

<!-- 📨 **Inria laboratory e-mail** `<first_name>.<last_name><at>inria.fr` -->

📨 **Professional out of lab'** `nicolas<dot>buton44<at>gmail.com`

## 👤 Work colleagues

👤 **Postdoc advisor** [Blaise Hanczar](https://sites.google.com/site/bhanczarhomepage/)

👤 **Postdoc advisor** [Farida Zehraoui](https://www.ibisc.univ-evry.fr/~zehraoui/)

👤 **Postdoc advisor** [Victoria Bourgeais](https://www.ibisc.univ-evry.fr/~vbourgeais/)


👤 **PhD Inria supervisor** [Francois.Coste](http://people.irisa.fr/Rumen.Andonov/)

👤 **PhD Inria supervisor** [Yann Le Cunff](https://www-dyliss.irisa.fr/team-members/yann-le-cunff/)

👤 **PhD thesis director** [Olivier Dameron](https://www-dyliss.irisa.fr/olivier-dameron/)

---

You can find my CV below partitioned into several categories.


## 📰 Publications

Nicolas Buton, François Coste, Yann Le Cunff, Predicting enzymatic function of protein sequences with attention. [Bioinformatics, 2023](https://doi.org/10.1093/bioinformatics/btad620)

## 🗣️ Talks

**GDR BIM - GT MASIM - 2022 Thematic meetings (ML & Sampling) (https://gt-masim.cnrs.fr/actions/thematic-meetings-ml-sampling/)**
* [Predicting enzymatic function of protein sequences with attention ](https://gt-masim.cnrs.fr/actions/thematic-meetings-ml-sampling/)

## 🪧 Poster
**2022 | Poster presentation at JOBIM**
* [EnzBert: Deep attention network for enzyme class predictions](https://hal.science/hal-03780557v1/file/jobim_2022_poster.pdf)

## 🏫 Teaching

**2021-2022 | ST1: Apprentissages Statistiques (TP), Master 1 – Univ. Rennes 1**
* Supervisor: Yann Le Cunff

**2020-2021 | ST1: Apprentissages Statistiques (TP), Master 1 – Univ. Rennes 1**
* Supervisor: Yann Le Cunff

**2020-2021 | Jury stage, Master 1 – Univ. Rennes 1**
* Supervisor: Annabelle Monnier

## 🎓 Education
**2020--2023 | PhD in bioinformatics**
* IRISA/University of Rennes - DYLISS team

**2018--2020 | Master degree in computer science specialized in machine learning with the highest honor**
* Données, Apprentissage, Connaissances (DAC), Sorbonne University

**2017--2018 | Bachelor degree in computer science (7th/110)**
* University of Rennes 1

**2018 | Toeic listening and reading - 900/990**
* Centre d'Examen à l'ESSCA 49000 ANGERS

**2015--2017 | CPGE scientifique MPSI-MP,  Preparation for national competitive entrance exams to leading French “grandes écoles”, specialising in mathematics and physics**
* Lycée Dupuy de Lôme Lorient

**2012--2015 | Science baccalaureate grades with honors**
* Lycée Alcide D’Orbigny

**2013 | Aeronautical Initiation Certificate (B.I.A. in french)**
* Aéroport Nantes Atlantique


## 🧑‍💻 WORK EXPERIENCE 
**2020-2023 | PhD student | INRIA - DYLISS team, Rennes, France**
* **Title:** Transformer models for interpretable and multilevel prediction of protein functions from sequence. 
* **Abstract:** Automatic annotation of protein sequences is on the rise to manage the increasing number of experimentally unannotated sequences. First, we investigated the application of the Transformer for enzymatic function prediction. The EnzBert model improves macro-F1 from 41% to 54% compared to the previous state-of-the-art. Furthermore, a comparison of interpretability methods shows that an attention-based approach achieves an F-Gain score of 96.05%, surpassing classical methods (91.44%). Second, the integration of Gene Ontology into function prediction models was explored. Two approaches were tested: integration in the labeling process and the use of hyperbolic embeddings. The results confirm both the effectiveness of the True Path Rule and the superiority of hyperbolic embeddings (mean WFmax: 0.36) compared to the Euclidean model (0.34) in low dimensions (32). They maintain greater consistency with the Gene Ontology (correctly ordered relations: 99.25%-99.28% vs. 78.48%-91.41% for the Euclidean model)

**2020 | Intern | INRIA - DYLISS team, Rennes, France**
* Study of the state of the art in protein characterization and create neural network inspired by the states of the art in NLP architecture, to characterize ptotein. And use domain knoledge to check and improve the network.

**2019 | Intern | MONDECA, Paris, France**
* Study of the state of the art in Natural Language Processing with deep learning. Implementation and test of Transformer network and LSTM with pytorch framework. In order to do multiple task like commonsense reasoning, semantic textual similarity, stance detection, open information extraction and entity linking.

**2018 | Intern | INRIA - LACODAM team, Rennes, France**
* Unsupervised learning for Intrusion Detection System (IDS), creation of auto encoder with Tensorflow and test of differents architecture and hyperparameters.

**2016 | Individual |Nantes**
* Individual tutoring in math for high school students


## 🥼 Services

**2023 | Vice treasurer in [Nicomaque](https://www.nicomaque-bretagne.fr/) PhD association**

**2022 | Organisation of “Journée au vert”**

**2022 | [JOBIM2022](https://jobim2022.sciencesconf.org/) Francophone conference**
* Volunteer at the JOBIM conference, creating badges, registering, welcoming and guiding participants around the conference site.


## 💬 Science popularisation

**2023 | [Volunteer for the recording of a conference on ethics in academia](https://atelier-ethique-inria2023.gitlab.io/)**
* [The recorded event (FR)](https://youtu.be/M8S5sF4kQGA?si=sd_Ri0N1W8wzlHEc)
* Recognised as scientific integrity and ethics training by Bretagne's PhD college

**2022 | Organisation of PhD popularisation film festival [Sciences en Cour[t]s](https://sciences-en-courts.fr/)**
* [The recorded event (FR)](https://youtu.be/1cyK35QFm7E)
* Collaboration with [Espace des Sciences](https://www.espace-sciences.org/) for the *Fête de la Science*

**2021 | Team member in [Sciences en Cour[t]s](https://sciences-en-courts.fr/) PhD popularisation film festival**
* Making a 5min animated film: [*Cocktail de bio-informatique*](https://youtu.be/4OVSAdGyuzs) (FR)


## 🏅 Programming competition

**2023 | [TRACS](https://tracs.viarezo.fr/)**

**2022 | [TRACS](https://tracs.viarezo.fr/)**
* CentraleSupélec, 91190 Gif-sur-Yvette, France
* First place in TRACS competition of Data science, cybersecurity, and cryptography

**2021 | [TRACS](https://tracs.viarezo.fr/)**

**2021 | [Google Hash Code](https://en.wikipedia.org/wiki/Hash_Code_(programming_competition))**

**2020 | [TRACS](https://tracs.viarezo.fr/)**

**2019 | [Les entrep'](https://www.lesentrep.fr/)**
* Paris, France
* Entrepreneurship competition with certificate.

**2018 | [Prologin, the French National Computer Science Contest](https://prologin.org/)**
* Paris, France
* Finalist of the national competition of computer science

**2018 | [24H du code](https://les24hducode.fr/)**
* La Mans, France
* Winner of the 6th edition

**2018 | [Google Hash Code](https://en.wikipedia.org/wiki/Hash_Code_(programming_competition))**

**2018 | [Google Code Jam](https://en.wikipedia.org/wiki/Google_Code_Jam)**

## 🚀 Projects
**2019 | Master 1 project - PLDAC**
* UPMC, Paris, France
* Brain Computer Interface(BCI)

**2016-2017 | TIPE (Work project in CPGE)**
* Lycée Dupuy de Lôme, Lorient, France
* Game theory / neural network / min-max / alpha-beta 

## 🖥️ Skills

### Programming languages

**Python3 (advanced)**
* **Project:** [TFPC: Transformer Framework for Protein Characterization](https://gitlab.inria.fr/nbuton/tfpc)
* **Python library:** Pytorch, Tensorflow, sklearn, pandas, torchmetrics, geoopt

**BASH** | **Java** | **R** | **C** |  **PHP** |  **SQL** | **JavaScript** | **Prolog** | **clips** | **whyml**

### Tools
**LaTeX** | **Markdown** | **Git**

### Languages

* **French** (native speaker)
* **English** (good working knowledge)
* **Spanish** (notions)

## Hobbies
**🏃‍♂️ Running**
* 4 mars 2023 | 21.1km | [Foulées Saint-Gilloises 2023](https://psgva.fr/semi-marathon-du-pays-de-saint-gilles/) | Injury
* 22 octobre 2022 | 10km | [La 10km Lamotte - marathon vert](https://www.lemarathonvert.org/) | 42:52 | 4min17
* 9 octobre 2022 | 10km | [Le 10km CMB - Tout Rennes Court 2022](http://www.toutrennescourt.fr/) | 43:05 | 4min18
* 28 aout 2022 | 21.1km | [Semi-marathon Cancale Saint-Malo](https://www.cancalestmalo.fr/) |  1:43:59 | 4min56
* 17 avril 2022 | 21,1km | [Semi-marathon de Nantes](https://marathondenantes.com/semi-marathon/) 1:50:17 | 5min14

**🚴‍♂️ Biking**
* 2015 | 3 days | 381km | Sainte-Pazanne > Sainte-Foy Triaize > Sainte-Pazanne
* 2016 | 4 days | 370km | Saint-Jean-de-Boiseau > Avrillé > Saumur > Saint Florent-le-Vieil > Sant-Jean-de-Boiseau
* 2017 | 4 days | 348km | La Montagne > Redon > Sulniac > La Turbale > Sainte-Pazanne 
* 2018 | 3 days | 182km | La Rochelle > Saint-Laurent-de-la-Prée > Royan > Around Île de Ré
* 2019 | 3 days | 222km | Saumur > Tours > Blois > Orléan
* 2020 | 4 days | 273km | Morlaix > Perros-Guirec > Paimpol > Saint Brieuc > Saint Malo
* 2021 | 3 days | 197m  | Angers > Château-Gontier > Laval > Mayenne > Laval 
* 2023 | 4 days | 210km | Saint-Jean-de-Boiseau > Saint-Brevin-les-Pins > Bourgneuf-en-Retz > La Barre-de-Monts > Les Sables-d'Olonne

**🥾 Hiking**
* 2023 | 6 days | 70km | Jura
* 2023 | 2 days | 80km | Belle-île
* 2022 | 4 days | 20km | Alps
* 2020 | Pyrenees

**🛩️ Aero modelling**
* At a club from 2008 to 2012

**🏸 Badminton**
* At a club in 2015 and 2022

**🎾 Tennis**
* At a club from 2007 to 2012

**🤾 Handball**
* At a club from 2005 to 2007

**🏀 Basketball**
* At a club from 2003 to 2005